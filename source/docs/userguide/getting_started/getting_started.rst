Getting Started
===============

Die App ``Agriculture Insurance`` ermöglicht es Felder und dazugehörige
Schäden anzulegen und zu verwalten.

Nähere Informationen finden Sie in den folgenden Anleitungen:

-  Anleitung zum Startbildschirm: :ref:`startscreen`
-  Anleitung zur Erstellung und Bearbeitung von
   Feldern: :ref:`field`
-  Anleitung zur Erstellung von Schäden :ref:`damage`
-  Anleitung zur Erstellung und Bearbeitung von
   Polygonen: :ref:`polygon`

Sie können die App sowohl in Portrait-Mode als auch im Landscape-Mode
bedienen.
