.. _damage:

Schäden
=======

Schadensverwaltung
------------------

Um die Verwaltung der Schäden zu öffnen, muss man das Menü öffnen und Schadensverwaltung
auswählen.

Hier werden alle gespeicherten Schäden nach Datum sortiert angezeigt. Gutachter
können alle Felder sehen, Landwirte dürfen natürlich nur die eigenen Felder
sehen.


Neuen Schaden erstellen
-----------------------

Sie können einen neuen Schaden über den Schnellzugriff auf der
Startseite erstellen oder über die Schadensverwaltung.

Neuen Schaden vom Startbildschirm aus erstellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Als Schnellzugriff gibt es direkt auf der Startseite der App einen ``+``
Button. Klicken Sie darauf und wählen Sie ``Schaden erstellen`` aus, um
einen neuen Schaden anzulegen.

Neuen Schaden von der Schadensverwaltung aus erstellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Um die Schadensverwaltung zu öffnen, klicken Sie links in der Toolbar
auf das Menu-Icon und wählen ``Schadensverwaltung`` aus.

Über den ``+`` Button können Sie dann direkt einen neuen Schaden
erstellen.

Neuen Schaden anlegen
~~~~~~~~~~~~~~~~~~~~~

Um einen neuen Schaden anzulegen, öffnet sich in beiden Fällen der
gleiche Dialog:

.. figure:: create_damage_dialog.png

Wenn Sie einen neuen Schaden erstellen, so müssen Sie das Feld angeben,
auf welchem der Schaden aufgetreten ist. Hier können Sie unter Ihren
bereits eingespeicherten Feldern wählen.

Bitte beachten Sie, dass es erst möglich ist, einen Schaden zu
erstellen, nachdem bereits ein Feld angelegt wurde, welchem der Schaden
zugeordnet werden kann. Falls Sie noch kein Feld angelegt haben, finden
Sie nähere Informationen dazu in unserer Anleitung zum Erstellen und
Bearbeiten von Feldern (:ref:`field`).

Um einen Schaden bei der Versicherung zu melden, müssen Sie immer auch
die Art des Schadensfalls angeben. Wählen Sie daher bei Schadenstyp den
entsprechenden Typ aus.

Über das ``+`` Icon oben rechts können Sie neue Schadenstypen erstellen.

.. figure:: new_damage_type.png

Sie können bei Schäden auch speichern, wann der Schaden aufgetreten ist.
Klicken Sie dazu ``Datum Auswählen`` und wählen Sie den Tag, an welchem
der Schaden aufgetreten ist. Mit einem Klick auf ``OK`` könne Sie Ihre
Auswahl bestätigen.

Um den Bereich des Schadens anzugeben, müssen Sie ein Polygon erstellen.
Klicken Sie dazu ``Schadensbereich markieren`` und folgen der Anleitung zur
Polygonerstellung (:ref:`polygon`).

.. attention::
  Beachten Sie bitte, dass der Schadensbereich innerhalb des Feldes liegen muss.

Für jeden Schaden können Sie eine Farbe wählen, mit welcher der Schaden
auf der Karte angezeigt wird.
Standardmäßig werden alle Schäden rot markiert.

Sie können zu ihrem Schaden ein Foto anlegen, um das Schadensausmaß
genau zu dokumentieren. Wählen Sie dazu ``Bild erstellen``.


Schadendetails
--------------

Um die gespeicherten Informationen zu einem Schaden ansehen zu können,
müssen Sie die Schadendetails öffnen.

.. figure:: damage_details.png

Dazu können Sie in Ihrer Kartenansicht auf den markierten Bereich des
Schadens klicken. Es öffnet sich dann die Detailansicht zu diesem Schaden.

Auch durch das Klicken auf den Schaden in der Übersicht der
Schadensverwaltung können Sie die Detailansicht öffnen.

Von der Detailansicht aus können Sie das Schaden bearbeiten, löschen und
die Farbe ändern.

Über die Toolbar können Sie auch direkt eine Navigation zum Schaden starten.

.. figure:: navigation.png

Zudem können Sie sich einen Kalendereintrag zur Schadensbegutachtung
automatisch erstellen lassen. Klicken Sie dazu auf das Kalender Icon.

.. figure:: calender.png

.. hint::
  Haben Sie mehrere Kalenderapps installiert, können Sie auswählen in welchem
  der Termin eingetragen werden soll.


Suche nach Schäden
------------------

Sie können in der Schadensverwaltung über die Suchleiste nach Schäden
suchen.

Suchen Sie dazu mit dem Namen des Feldes, zudem Sie die zugehörigen
Schäden angezeigt bekommen wollen. Sie können aber auch nach dem Namen
des Landwirts suchen. Dann werden Ihnen alle Schäden angezeigt, welche
diesem Landwirt zugeordnet sind.
