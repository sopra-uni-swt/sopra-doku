.. _startscreen:

Startbildschirm
===============

Der Startbildschirm zeigt auf einer Karte Ihre erstellten Felder und
Schäden.

.. figure:: startscreen_field_overview.png
   :alt: startscreen\_field\_overview

Falls Sie noch keine Felder oder Schäden angelegt haben, lesen Sie am
besten unsere Anleitung zum Erstellen von Feldern (:ref:`field`) und zum Erstellen
von Schäden (:ref:`damage`).

Durch Wisch-Gesten können Sie die Karte verschieben und mit
Pinch-To-Zoom können Sie in den angezeigten Kartenausschnitt rein oder
rauszommen.

Durch Klicken auf das Positions-Icon über dem ``+`` Button können Sie
die Karte auf Ihre aktuelle Position zentrieren.

Offline Karten
--------------

Selbstverständlich können Sie die App vollständig offline nutzen.
Allerdings können offline nicht immer die Kartendaten angezeigt werden.
Um zusätzlich auch offline die Kartendaten zur Verfügung zu haben,
können Sie diese über Google Maps herunterladen.

Öffnen Sie dazu die Google Maps App und melden Sie sich mit Ihrem Google
Account an. Öffnen Sie dann das seitliche Menü.

.. figure:: google_maps_offline_menue.png
   :alt: google\_maps\_offline\_menue

Es öffnet sich dann das Fenster um offline Bereiche auszuwählen.

.. figure:: google_maps_offline_areas.png
   :alt: google\_maps\_offline\_areas

Klicken Sie auf ``Custom area``. Hier können Sie den Kartenbereich
wählen, welchen Sie offline zur Verfügung haben möchten und diesen dann
herunterladen.

.. figure:: google_maps_offline_select_custom_area.png
   :alt: google\_maps\_offline\_select\_custom\_area
