.. _field:

Felder
======

Feldverwaltung
--------------

Um die Verwaltung der Felder zu öffnen, muss man das Menü öffnen und Feldverwaltung
auswählen.

.. figure:: menu.png

Hier werden alle gespeicherten Feldern nach Namen sortiert angezeigt. Gutachter
können alle Felder sehen, Landwirte dürfen natürlich nur die eigenen Felder
sehen.

Neues Feld erstellen
--------------------

Es gibt zwei Möglichkeiten ein neues Feld anzulegen:

Neues Feld vom Startbildschirm aus erstellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Als Schnellzugriff gibt es direkt auf der Startseite der App einen ``+``
Button. Klicken Sie darauf und wählen Sie ``Feld erstellen`` aus, um ein
neues Feld anzulegen.

.. figure:: start_newfield.png

Neues Feld von der Feldverwaltung aus erstellen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Um die Feldverwaltung zu öffnen, klicken Sie links in der Toolbar auf
das Menu-Icon und wählen ``Feldverwaltung`` aus.

Über den ``+`` Button können Sie dann direkt ein neues Feld erstellen.

Neues Feld
~~~~~~~~~~

Um das neue Feld anzulegen, öffnet sich in beiden Fällen der gleiche
Dialog:

.. figure:: newfield.png

Hier können Sie dem Feld einen Namen geben.

Zudem müssen Sie für jedes Feld ein Feldbereich markieren. Dieser
speichert dann die genauen Eckpunkte Ihres Feldes. Eine genaue Anleitung
zum Markieren eines Feldbereiches finden sie hier: :ref:`polygon`.

.. attention::
  Verschiedene Felder dürfen sich nicht überlappen.

Sie können Ihr neues Feld erst speichern, wenn Sie dazu auch einen Bereich markiert
haben.

Die Farbe mit der das Feld auf Ihrer Karte angezeigt wird, lässt sich
beim Erstellen des Feldes auswählen. Wählen Sie dazu
``Farbe auswählen``.

.. figure:: selectcolor.png

Standardmäßig werden alle Felder grün markiert.

Zudem können Sie zu einem Feld ein Foto speichern. Klicken Sie
``Bild hinzufügen`` und wählen Sie, ob Sie ein neues Foto aufnehmen wollen,
oder eines von ihrem Handy laden wollen. Die Anzahl der Bilder wird ihnen
angezeigt. Klicken Sie auf das Feld ``Bilder anzeigen`` um die hinzugefügten
Aufnahmen anzusehen und zu bearbeiten.


Felddetails
-----------

Um die gespeicherten Informationen zu einem Feld ansehen zu können,
müssen Sie die Felddetails öffnen.

.. figure:: fielddetails.png

Dazu können Sie in Ihrer Kartenansicht auf den markierten Bereich des
Feldes klicken. Es öffnet sich dann die Detailansicht zu diesem Feld.

Auch durch das Klicken auf den Feldnamen in der Übersicht der
Feldverwaltung können Sie die Detailansicht öffnen.

Von der Detailansicht aus können Sie das Feld bearbeiten, löschen und
die Farbe ändern.

Ändern der Feldfarbe
~~~~~~~~~~~~~~~~~~~~

Um die Farbe eines Feldes zu ändern, klicken Sie auf das Icon mit 3
Punkten an der rechten Seite der Toolbar und wählen ``Farbe ändern``
aus.

Bearbeiten eines existierenden Feldes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Um ein bereits erstellte Felder zu bearbeiten, öffnen Sie die
Felddetails Ansicht zu dem Feld, welches Sie bearbeiten möchten und
klicken auf des Stift Icon in der Toolbar.

Sie können hier, wie beim Erstellen des Feldes, das Polygon, die Farbe
und das Bild bearbeiten.

Klicken Sie auf Speichern, um Ihre Änderungen zu übernehmen. Möchten Sie
die Änderungen verwerfen, müssen Sie auf den Zurück-Pfeil in der oberen
linken Ecke klicken.

Löschen eines Feldes
~~~~~~~~~~~~~~~~~~~~

Um ein gespeichertes Feld zu löschen, klicken die auf das Papierkorb
Icon in der Toolbar der Felddetails Ansicht.

Ein Dialog Fenster frägt dann ab, ob Sie sich sicher sind, dass Sie das
Feld löschen wollen.

Suche
-----

Sie können in der Feldverwaltung mit Stichworten die Namen der Felder
durchsuchen und nach dem zugehörigen Landwirt zu einem Feld suchen.

Geben Sie dazu in der Suchleiste der Feldverwaltung Ihren Suchbegriff
ein. Das Bestätigen des Suchbegriffs führt dazu, dass nur noch Felder
angezeigt werden, deren Namen oder Landwirt auf den Suchbegriff passt.
