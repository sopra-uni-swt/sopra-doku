Einstellungen
=============

Öffnen Sie über das Menü die Einstellungsseite.

.. figure:: settings.png

Hier können Sie die Einheit der Fläche und die Kartenansicht wählen.

Über die Einstellung ``Getting Started Guide`` können Sie die Interaktive
Bedienungshilfe aktivieren und deaktivieren.

.. hint::
  Dieser Guide wird nach erstmaligem erfolgreichen fertigstellen automatisch deaktiviert.

Klicken Sie ``Dokumentation öffnen`` so öffnet sich diese Website.

.. danger:: Es wird eine Internetverbindung benötigt, um die Dokumentation zu lesen.
