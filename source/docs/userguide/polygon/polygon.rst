.. _polygon:

Polygon
=======

Erstellen eines Polygons
------------------------

Nachdem Sie beim Erstellen eines Feldes oder Schadens auf
``Bereich markieren`` geklickt haben, öffnet sich die Ansicht zum
Erstellen des Polygons.

Hier können Sie durch Klicken auf den Bildschirm einen neuen Eckpunkt
hinzufügen.

Möchten Sie als nächsten Eckpunkt Ihre aktuelle Position wählen, so
klicken Sie auf das Positionsicon über dem Speichern-Button.

Die Eckpunkte werden der Reihenfolge nach verbunden, in der Sie
eingespeichert wurden. Dabei wird immer der zuletzt eingetragene
Eckpunkt mit dem ersten Eckpunkt verbunden.

.. figure:: create_polygon.png

Möchten Sie einen Eckpunkt wieder löschen, so müssen Sie erneut darauf
klicken. Der Eckpunkt wird dann entfernt.

Haben Sie einen Eckpunkt bereits platziert und möchten diesen
verschieben, so müssen die Eckpunkt klicken und halten, bis sich dieser
vom Polygon löst. Sie können den Eckpunkt dann nach belieben
verschieben. Sobald Sie den Eckpunkt loslassen wird dieser an der
aktuellen Position neu platziert.

Speichern der Markierung
------------------------

Um Ihr erstellte Markierung zu Speichern, müssen die auf den Speichern
Button klicken.

Bitte beachten Sie, dass man nur valide Markierungen speichern darf. Um
valide zu sein, muss ein Polygon mindestens 3 Eckpunkte besitzen und die
Kanten dürfen keine Überschneidungen haben.

Ist Ihr eingegebenes Polygon nicht valide, so werden beim Erstellen die
Linien gestrichelt markiert und die Fläche wird nicht ausgefüllt.

.. figure:: invalidpolygon.png

Versuchen Sie ein solches nicht valides Polygon zu speichern, so ist das
nicht möglich, und die werden mit einem Warnhinweis nochmals darauf
hingewiesen, warum ihr Polygon nicht gespeichert werden kann.

Bearbeiten eines Polygons
-------------------------

Haben Sie bereits ein Polygon zu einem Feld oder Schaden erstellt und
den Button ``Bereich Bearbeiten`` ausgewählt, so wird Ihr bereits
gespeichertes Polygon auf der Karte angezeigt. Sie können wie bei der
Erstellung des Polygons durch Klicken neue Eckpunkte hinzufügen oder
bestehende entfernen. Halten Sie einen Eckpunkt, so kann dieser
verschoben werden.

Über den Speicher Button können Sie Ihre Änderungen speichern. Auch beim
Bearbeiten ist es nur möglich valide Polygone zu speichern. Möchten Sie
die Änderungen nicht speichern, so können Sie diese mit dem
Zurück-Button verwerfen.
